#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import csv
from calcoohija import CalculadoraHija


if __name__ == '__main__':
    op1 = sys.argv[1]
    op2 = op1
    Calc = CalculadoraHija(op1, op2)
    with open(sys.argv[1], newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        for row in reader:
            Operation = row.pop(0)
            try:
                if Operation == 'suma':
                    Result = 0
                    for Op in row:
                        Result = Result + Calc.plus(int(Op), 0)
                    print(Result)
                elif Operation == 'resta':
                    Result = int(row.pop(0))
                    Sum = 0
                    for Op in row:
                        # A-B-C-D = A - (B + C + D)
                        # Restamos al primer numero la suma del resto
                        Sum = Sum + Calc.minus(int(Op), 0)
                    Result = Result - Sum
                    print(Result)
                elif Operation == 'multiplica':
                    Result = 1
                    for Op in row:
                        Result = Result*Calc.multiplicar(int(Op), 1)
                    print(Result)
                elif Operation == 'divide':
                    Result = int(row.pop(0))
                    try:
                        for Op in row:
                            Result = Result/Calc.dividir(int(Op), 1)
                        print(Result)
                    except ZeroDivisionError:
                        sys.exit('Division by zero is not allowed')
                else:
                    sys.exit('Possible operations: /suma/resta/'
                             'divide/multiplica/')
            except ValueError:
                sys.exit("Non numerical parameters")
