#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from calcoo import Calculadora


class CalculadoraHija(Calculadora):
    def dividir(self, op1, op2):
        if self.op2 == 0:
            sys.exit("Division by zero is not allowed")
        else:
            return (op1/op2)

    def multiplicar(self, op1, op2):
        return (op1 * op2)


if __name__ == "__main__":
    try:
        op1 = int(sys.argv[1])  # Para que nos lo recoja a la hora de llamarlo
        op2 = int(sys.argv[3])
        c = Calculadora(op1, op2)  # Creamos un objeto heredado de calculadora
        d = CalculadoraHija(op1, op2)  # Creamos un objeto de calcudorahija
    except ValueError:
        # Excepción para que nos salte si no introducciomos un número
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma" or sys.argv[2] == "sumar":
        result = c.plus(op1, op2)
    elif sys.argv[2] == "resta" or sys.argv[2] == "restar":
        result = c.minus(op1, op2)
    elif sys.argv[2] == "divide" or sys.argv[2] == "dividir":
        result = d.dividir(op1, op2)
    elif sys.argv[2] == "multiplicar" or sys.argv[2] == "multiplica":
        result = d.multiplicar(op1, op2)
    else:
        # Error que nos saltara si introducimos cualquier cosa que no sea eso.
        sys.exit('Operación sólo puede ser sumar, '
                 'restar, dividir o multiplicar.')
    print(result)
